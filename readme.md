# Kickstarter V5
## For rapid template development

- Include JADE Template Engine for easy developing Clickdummys and Mockups
- Best of use with Codekit

New:

- Use Icomoon (http://icomoon.io/app/) for establish the Iconfont. Its easy to use and you get not the big Overhead.
- Included Iconfont Mixins, so its easy.

Workflow with Iconmoon Iconfont:

- You place your IconFont with the app together
- Download the Package
- Copy the fonts in the Font Directory
- Open the Style.css and copy all clases in the File (lib/iconfont/_icons.scss) ONLY the Classes with Declaration
- When IE7 is needed open the File ie7.sass and use the Mixin "iconfontIE7" for all Icons. Its a little Handwork but it works.