jQuery(document).ready(function ($) {

	// Mobile Navigation --------------------------------------------------
	if(!$.browser.msie && !parseFloat($.browser.version) < 9){
		// Mobile Navigation Switcher
		if($(window).width() <= 479) {
			$('#mainNav ul').addClass('hiddenPhone');
		} 

		function checkWidth() {
			var windowsize = $(window).width();
			if(windowsize <= 479) 
			{
				$('#mainNav ul').addClass('hiddenPhone').hide();

			} 
			else 
			{
				$('#mainNav ul').removeClass('hiddenPhone').show();
			}
		}

		$('.mobileNavButton').click(function() {
			if($(this).hasClass('active')) 
			{
				$(this).removeClass('active');
				$('#mainNav ul').slideUp(function(){
					$(this).attr('style', 'display: inherit');
				});
			} 
			else 
			{
				$(this).addClass('active');
				$('#mainNav ul').slideDown(function(){
					$(this).attr('style', 'display: block !important')
				});
			}
		});
		$(window).resize(checkWidth);
	} 
	
	// TABS --------------------------------- 
	// function activateTab($tab) {
	// 	var $activeTab = $tab.closest('dl').find('a.active'),
	// 			contentLocation = $tab.attr("href") + 'Tab';

	// 	//Make Tab Active
	// 	$activeTab.removeClass('active');
	// 	$tab.addClass('active');

	// 	//Show Tab Content
	// 	$(contentLocation).closest('.tabs-content').children('li').hide();
	// 	$(contentLocation).show();
	// }
	// $('dl.tabs').each(function () {
	// 	//Get all tabs
	// 	var tabs = $(this).children('dd').children('a');
	// 	tabs.click(function (e) {
	// 		activateTab($(this));
	// 	});
	// });
	// if (window.location.hash) {
	// 	activateTab($('a[href="' + window.location.hash + '"]'));
	// }

	// ALERT BOXES ------------
	// $(".alert-box").delegate("a.close", "click", function(event) {
	// 	event.preventDefault();
	// 	$(this).closest(".alert-box").fadeOut(function(event){
	// 		$(this).remove();
	// 	});
	// });

	// Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
	// if (Modernizr.touch && !window.location.hash) {
	// 	$(window).load(function () {
	// 	  setTimeout(function () {
	// 	    window.scrollTo(0, 1);
	// 	  }, 0);
	// 	});
	// }

	// DROPDOWN NAV ------------- 
	// var lockNavBar = false;
	// $('.nav-bar a.flyout-toggle').live('click', function(e) {
	// 	e.preventDefault();
	// 	var flyout = $(this).siblings('.flyout');
	// 	if (lockNavBar === false) {
	// 		$('.nav-bar .flyout').not(flyout).slideUp(500);
	// 		flyout.slideToggle(500, function(){
	// 			lockNavBar = false;
	// 		});
	// 	}
	// 	lockNavBar = true;
	// });

	// PLACEHOLDER FOR FORMS ------------- 
	// $('input, textarea').placeholder();

	// TOOLTIPS -------------
	// $(this).tooltips();

	// FLEXSLIDER -------------
	// $('.flexslider').flexslider({
	// 	animation: "slide",
	// 	controlsContainer: ".flex-container"
	// });

    // FANCYBOX -------------
    // http://fancyapps.com/fancybox/
	// $(".fancybox").fancybox({
	// 	closeEffect : 'elastic',
	// 	padding     : '4',
	// 	helpers: {
	// 		title : {
	// 			type: 'inside'
	// 	},
	// 		overlay	: {
	// 			opacity : 0.6,
	// 			css : {
	// 				'background-color' : '#000'
	// 			}
	// 		}
	// 	}
	// });

	// SMALLSCRIPT FOR THE CSS3 IMAGEWRAPPER ------------- 
	// $('.img3d').each(function() {
	// 	var imgClass = $(this).attr('class');
	// 	$(this).wrap('<span class="image-wrap ' + imgClass + '" style="width: auto; height: auto;"/>');
	// 	$(this).removeAttr('class');
	// });

	// RESPONSIVE TOUCH OPTIMIZED NAVIGATION ------------- 
	// https://github.com/samuelcotterall/touchdown 
	// $('YOURNAVIGATION ID OR CLASS').Touchdown();

	// SCROLLPANE INIT -------------
	// $('.CLASS OR ID WITH SCROLLING CONTENT').jScrollPane();

	// Init Background Stretcher ---
	// $('#bgstretch-container').bgStretcher({
	// 	images           : ['images/1.jpg', 'images/2.jpg', 'images/3.jpg'],
	// 	imageWidth       : 1024, 
	// 	imageHeight      : 768, 
	// 	slideDirection   : 'W',
	// 	nextSlideDelay   : 8000,
	// 	slideShowSpeed   : 6000,
	// 	transitionEffect : 'fade',
	// 	sequenceMode     : 'normal',
	// 	buttonPrev       : '#prev',
	// 	buttonNext       : '#next',
	// 	pagination       : '#nav',
	// 	anchoring        : 'left center',
	// 	anchoringImg     : 'left center'
	// });

	// Hybrid Scroll Fixed Navigation --------------------
	// var $win = $(window)
	// 	, $nav = $('#site-navigate')
	// 	, navTop = $('#site-navigate').length && $('#site-navigate').offset().top - 10
	// 	, isFixed = 0
	// processScroll()
	// $win.on('scroll', processScroll)
	// function processScroll() {
	// 	var i, scrollTop = $win.scrollTop()
	// 	if (scrollTop >= navTop && !isFixed) {
	// 		isFixed = 1
	// 		$nav.addClass('site-navigate-fixed')
	// 	} else if (scrollTop <= navTop && isFixed) {
	// 		isFixed = 0
	// 		$nav.removeClass('site-navigate-fixed')
	// 	}
	// }

	// Footer Slide Up ------------------------------
	// var slide = false;
	// var height = $('#footer').height();
	// $('#footer-button').click(function() {
	// 	var docHeight = $(document).height();
	// 	var windowHeight = $(window).height();
	// 	var scrollPos = docHeight - windowHeight + height;
	// 	$('#footer').animate({ height: "toggle"}, 1000);
	// 	if(slide == false) {
	// 		if($.browser.opera) { //Fix opera double scroll bug by targeting only HTML.
	// 			$('html').animate({scrollTop: scrollPos+'px'}, 1000);
	// 		} else {
	// 			$('html, body').animate({scrollTop: scrollPos+'px'}, 1000);
	// 		}
	// 		slide = true;
	// 	} else {
	// 		slide = false;
	// 	}
	// });
	
	// Responsive Images
	// $(function(){
	//     $('figure.responsiveImage, picture, figure').picture();
	// });
});